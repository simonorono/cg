package com.mcbodev.cg

import com.mcbodev.cg.lexer.Lexer
import com.mcbodev.cg.lexer.TokenType

const val PROMPT = ">>> "

object REPL {
    fun start() {
        val reader = System.`in`.bufferedReader()

        while (true) {
            print(PROMPT)

            val input = reader.readLine()

            if (input.isEmpty()) {
                return
            }

            val lexer = Lexer(input)

            var tok = lexer.nextToken()

            while (tok.type != TokenType.EOF) {
                println(tok)

                tok = lexer.nextToken()
            }
        }
    }
}
