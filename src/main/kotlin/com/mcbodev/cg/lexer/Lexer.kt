package com.mcbodev.cg.lexer

import java.io.PushbackReader

class Lexer(private val inputReader: PushbackReader) {
    private var currentChar: Char = 0.toChar()

    init {
        readChar()
    }

    constructor(input: String) : this(
        PushbackReader(
            input.byteInputStream().bufferedReader()
        )
    )

    private fun readChar() {
        currentChar = inputReader.read().toChar()
    }

    private fun peekChar(): Char {
        val char = inputReader.read()
        inputReader.unread(char)

        return char.toChar()
    }

    private fun newSingleCharToken(type: TokenType) =
        Token(type, "$currentChar")

    private fun skipWhitespace() {
        while (currentChar.isWhitespace()) {
            readChar()
        }
    }

    /**
     * Reads characters as long as predicate returns `true` for [currentChar].
     *
     * @return all characters for which the predicate returned `true`.
     */
    private fun readTokenWhile(predicate: (Char) -> Boolean): String {
        var string = ""

        while (predicate(currentChar)) {
            string += currentChar
            readChar()
        }

        return string
    }

    fun nextToken(): Token {
        skipWhitespace()

        val token = when (currentChar) {
            '=' -> {
                if (peekChar() == '=') {
                    readChar()
                    Token(TokenType.EQUAL, "==")
                } else {
                    newSingleCharToken(TokenType.ASSIGN)
                }
            }
            '!' -> {
                if (peekChar() == '=') {
                    readChar()
                    Token(TokenType.NOT_EQUAL, "!=")
                } else {
                    newSingleCharToken(TokenType.BANG)
                }
            }

            '+' -> newSingleCharToken(TokenType.PLUS)
            '-' -> newSingleCharToken(TokenType.MINUS)
            '*' -> newSingleCharToken(TokenType.ASTERISK)
            '/' -> newSingleCharToken(TokenType.SLASH)
            '<' -> newSingleCharToken(TokenType.LT)
            '>' -> newSingleCharToken(TokenType.GT)
            '(' -> newSingleCharToken(TokenType.LPAREN)
            ')' -> newSingleCharToken(TokenType.RPAREN)
            '{' -> newSingleCharToken(TokenType.LBRACE)
            '}' -> newSingleCharToken(TokenType.RBRACE)
            ',' -> newSingleCharToken(TokenType.COMMA)
            ';' -> newSingleCharToken(TokenType.SEMICOLON)

            (-1).toChar() -> Token(TokenType.EOF, "")

            else -> {
                when {
                    currentChar.isLetter() -> {
                        val word = readTokenWhile { it.isLetter() || it == '_' }
                        return Token(TokenType.lookupIdent(word), word)
                    }

                    currentChar.isDigit() -> {
                        val number =
                            readTokenWhile { it.isDigit() || it == '.' }

                        val dotCount = number.count { it == '.' }

                        if (dotCount > 1) {
                            return Token(TokenType.ILLEGAL, number)
                        }

                        return Token(
                            if (dotCount == 1) TokenType.FLOAT else TokenType.INT,
                            number
                        )
                    }

                    else -> newSingleCharToken(TokenType.ILLEGAL)
                }
            }
        }

        readChar()
        return token
    }
}
