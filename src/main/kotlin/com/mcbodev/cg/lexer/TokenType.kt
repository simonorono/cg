package com.mcbodev.cg.lexer

enum class TokenType {
    // special
    ILLEGAL,
    EOF,

    // identifiers & literals
    IDENT,
    INT,
    FLOAT,

    // operators
    ASSIGN,
    PLUS,
    MINUS,
    BANG,
    ASTERISK,
    SLASH,
    LT,
    GT,
    EQUAL,
    NOT_EQUAL,

    // delimiters
    COMMA,
    SEMICOLON,

    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,

    // keywords
    FUNCTION,
    LET,
    IF,
    ELSE,
    RETURN,
    TRUE,
    FALSE;

    companion object {
        private val keywordsMap = mapOf(
            "fn" to FUNCTION,
            "let" to LET,
            "if" to IF,
            "else" to ELSE,
            "return" to RETURN,
            "true" to TRUE,
            "false" to FALSE,
        )

        fun lookupIdent(literal: String) = keywordsMap[literal] ?: IDENT
    }
}
