package com.mcbodev.cg.lexer

data class Token(
    val type: TokenType,
    val literal: String
)
