rootProject.name = "cg"

pluginManagement {
    repositories {
        maven(url = "https://nexus.gluonhq.com/nexus/content/repositories/releases")
        gradlePluginPortal()
    }
}
